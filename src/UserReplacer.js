import {ApplicationService, Args} from "@themost/common";
import {
    DataPermissionEventListener,
    EdmMapping,
    EdmType,
    ModelClassLoaderStrategy,
    SchemaLoaderStrategy
} from "@themost/data";
import {promisify} from 'util';

export class User {
    @EdmMapping.func('Permissions', EdmType.CollectionOf('Permission'))
    static getPermissions(context) {
        return context.model('Permission').asQueryable();
    }

    @EdmMapping.param('items', 'Object', false, true)
    @EdmMapping.action('Permissions', 'Object')
    static setPermissions(context, items) {
        return context.model('Permission').save(items);
    }

    @EdmMapping.param('template', 'Object', false, true)
    @EdmMapping.action('HasPermission', 'Object')
    async hasPermission(template) {
        Args.notString(template.model, 'Target model');
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const finalTemplate = Object.assign({}, template);
        // convert model to DataModel
        finalTemplate.model = this.context.model(finalTemplate.model);
        const validateAsync = promisify(validator.validate).bind(validator);
        await validateAsync(Object.assign(finalTemplate, { throwError: false}));
        return {
            value: finalTemplate.result
        };
    }
}

export class UserReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('User');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const UserClass = loader.resolve(model);
        // set user extensions
        UserClass.getPermissions = User.getPermissions;
        UserClass.setPermissions = User.setPermissions;
        UserClass.prototype.hasPermission = User.prototype.hasPermission;
    }

}
